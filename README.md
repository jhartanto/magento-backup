magento-backup
==============

This script will create a file and database dump of a Magento application.

###Features

 * Automatically enters database credentials
 * Ignores contents of log tables
 * Obfuscates customer information

###Usage

    cd /path/to/magento/
    wget https://bitbucket.org/jhartanto/magento-backup/raw/7d6a3b2fb83c0ebcafd67971c2dc7ee818b6912e/backup.sh
    sh backup.sh

###Credits

This script was originally developed by the Magento Core team.
